


                     eT 1.7 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, R. Matveeva, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, 
   E. RoncaM. Scavino, A. K. Schnack-Petersen, A. S. Skeidsvoll, Å. 
   H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.7.0 Hydra
  ------------------------------------------------------------
  Configuration date: 2022-04-07 12:33:31 UTC -07:00
  Git branch:         release-v1.7.0
  Git hash:           c5c382d7d23365463896a87c37298e4a37e4c962
  Fortran compiler:   GNU 11.2.0
  C compiler:         GNU 11.2.0
  C++ compiler:       GNU 11.2.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2022-04-07 12:34:38 UTC -07:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
         name: visualize
         charge: 0
         cartesian gaussians
     end system

     do
        ground state
     end do

     method
         hf
     end method

     memory
        available: 8
     end memory

     solver scf
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
     end solver scf

     visualization
        plot hf orbitals: [3,5]
        file format: cube
        grid spacing: 0.4
     end visualization


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  H    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  H    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               25
     Number of orthonormal atomic orbitals:   25

  - Molecular orbital details:

     Number of occupied orbitals:         5
     Number of virtual orbitals:         20
     Number of molecular orbitals:       25


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)
     3) Plot orbitals and/or density


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - Setting initial AO density to sad

     Energy of initial guess:               -75.634935703586
     Number of electrons in guess:           10.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-16
     Exchange screening threshold:   0.1000E-14
     ERI cutoff:                     0.1000E-16
     One-electron integral  cutoff:  0.1000E-21
     Cumulative Fock threshold:      0.1000E+01

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -75.943240511599     0.1098E+00     0.7594E+02
     2           -75.975120506787     0.6982E-01     0.3188E-01
     3           -75.989819187113     0.6910E-02     0.1470E-01
     4           -75.990141078142     0.2719E-02     0.3219E-03
     5           -75.990177721761     0.3797E-03     0.3664E-04
     6           -75.990178781594     0.5645E-04     0.1060E-05
     7           -75.990178804287     0.6047E-05     0.2269E-07
     8           -75.990178805012     0.2102E-05     0.7251E-09
     9           -75.990178805114     0.3527E-06     0.1023E-09
    10           -75.990178805115     0.2526E-07     0.5400E-12
    11           -75.990178805115     0.6579E-08     0.4263E-13
    12           -75.990178805115     0.5937E-09     0.5684E-13
    13           -75.990178805115     0.9334E-10     0.5684E-13
    14           -75.990178805115     0.2106E-10     0.4263E-13
    15           -75.990178805115     0.6103E-11     0.7105E-13
  ---------------------------------------------------------------
  Convergence criterion met in 15 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.642404183413
     Nuclear repulsion energy:       8.002366974166
     Electronic energy:            -83.992545779281
     Total energy:                 -75.990178805115


  3) Plot orbitals and/or density

  :: Visualization of orbitals and density

     Grid information              x             y             z
     ------------------------------------------------------------------
     First (A):                   -2.87         -2.08          3.00
     Last (A):                     2.33          2.32          6.60
     Number of grid points:        14            12             10
     ------------------------------------------------------------------

  - Placing the AOs evaluated on the grid in memory

  - Plotting orbitals

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.31000
     Total cpu time (sec):               0.38766

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 425.064 KB

  Total wall time in eT (sec):              0.32100
  Total cpu time in eT (sec):               0.39851

  Calculation ended: 2022-04-07 12:34:38 UTC -07:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
