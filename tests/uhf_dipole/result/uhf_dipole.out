


                     eT 1.7 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, R. Matveeva, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, 
   E. RoncaM. Scavino, A. K. Schnack-Petersen, A. S. Skeidsvoll, Å. 
   H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.7.0 Hydra
  ------------------------------------------------------------
  Configuration date: 2022-04-07 12:33:31 UTC -07:00
  Git branch:         release-v1.7.0
  Git hash:           c5c382d7d23365463896a87c37298e4a37e4c962
  Fortran compiler:   GNU 11.2.0
  C compiler:         GNU 11.2.0
  C++ compiler:       GNU 11.2.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2022-04-07 12:34:39 UTC -07:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: HOH He
        charge: 0
        multiplicity: 3
     end system

     method
        uhf
     end method

     memory
        available: 8
     end memory

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
     end solver scf

     do
        ground state
     end do

     hf mean value
        dipole
     end hf mean value


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: UHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  H    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  H    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               29
     Number of orthonormal atomic orbitals:   29

  - Molecular orbital details:

     Number of alpha electrons:               7
     Number of beta electrons:                5
     Number of virtual alpha orbitals:       22
     Number of virtual beta orbitals:        24
     Number of molecular orbitals:           29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a UHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)
     3) Calculate dipole and/or quadrupole moments


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - Setting initial AO density to sad

     Energy of initial guess:               -78.755091995809
     Number of electrons in guess:           12.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-16
     Exchange screening threshold:   0.1000E-14
     ERI cutoff:                     0.1000E-16
     One-electron integral  cutoff:  0.1000E-21
     Cumulative Fock threshold:      0.1000E+01

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.636386138863     0.2992E-01     0.7864E+02
     2           -78.653293795584     0.1270E-01     0.1691E-01
     3           -78.655536952313     0.5188E-02     0.2243E-02
     4           -78.656095298521     0.2680E-02     0.5583E-03
     5           -78.656255205544     0.1181E-02     0.1599E-03
     6           -78.656314613449     0.4048E-03     0.5941E-04
     7           -78.656324112960     0.9337E-04     0.9500E-05
     8           -78.656324376053     0.1512E-04     0.2631E-06
     9           -78.656324387940     0.9966E-05     0.1189E-07
    10           -78.656324392031     0.8514E-05     0.4091E-08
    11           -78.656324400673     0.4014E-05     0.8642E-08
    12           -78.656324403810     0.6173E-06     0.3137E-08
    13           -78.656324403929     0.4504E-06     0.1192E-09
    14           -78.656324403966     0.3797E-06     0.3674E-10
    15           -78.656324403999     0.2984E-06     0.3286E-10
    16           -78.656324404021     0.1161E-06     0.2268E-10
    17           -78.656324404025     0.5464E-07     0.3482E-11
    18           -78.656324404025     0.4187E-07     0.1990E-12
    19           -78.656324404025     0.2086E-07     0.7105E-13
    20           -78.656324404025     0.3529E-08     0.5684E-13
    21           -78.656324404025     0.9043E-09     0.2842E-13
    22           -78.656324404025     0.3486E-09     0.5684E-13
    23           -78.656324404025     0.1300E-09     0.7105E-13
    24           -78.656324404025     0.9426E-10     0.7105E-13
    25           -78.656324404025     0.5441E-10     0.5684E-13
    26           -78.656324404025     0.2286E-10     0.2842E-13
    27           -78.656324404025     0.5591E-11     0.5684E-13
  ---------------------------------------------------------------
  Convergence criterion met in 27 iterations!

  - Summary of UHF wavefunction energetics (a.u.):

     HOMO-LUMO gap (alpha):          0.329592118296
     HOMO-LUMO gap (beta):           0.699081248367
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.772424978612
     Total energy:                 -78.656324404025

  - UHF wavefunction spin expectation values:

     Sz:                   1.00000000
     Sz(Sz + 1):           2.00000000
     S^2:                  2.00764752
     Spin contamination:   0.00764752


  3) Calculate dipole and/or quadrupole moments

  - Operator: dipole moment [a.u.]

     x:          0.0089001
     y:         -0.0535825
     z:          0.0209490

     |mu|:       0.0582165

  - Operator: dipole moment [Debye]

     x:          0.0226219
     y:         -0.1361932
     z:          0.0532469

     |mu|:       0.1479715

  - Timings for the UHF ground state calculation

     Total wall time (sec):              1.21900
     Total cpu time (sec):               1.12055

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 302.076 KB

  Total wall time in eT (sec):              1.23000
  Total cpu time in eT (sec):               1.13111

  Calculation ended: 2022-04-07 12:34:41 UTC -07:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
