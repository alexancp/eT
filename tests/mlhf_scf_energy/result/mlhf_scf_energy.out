


                     eT 1.7 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, R. Matveeva, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, 
   E. RoncaM. Scavino, A. K. Schnack-Petersen, A. S. Skeidsvoll, Å. 
   H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.7.0 Hydra
  ------------------------------------------------------------
  Configuration date: 2022-04-07 12:33:31 UTC -07:00
  Git branch:         release-v1.7.0
  Git hash:           c5c382d7d23365463896a87c37298e4a37e4c962
  Fortran compiler:   GNU 11.2.0
  C compiler:         GNU 11.2.0
  C++ compiler:       GNU 11.2.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2022-04-07 12:34:41 UTC -07:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: 2 H2O close
        charge: 0
        multiplicity: 1
     end system

     do
        ground state
     end do

     memory
        available: 8
     end memory

     solver scf
        algorithm: scf
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
     end solver scf

     method
        mlhf
     end method

     active atoms
        selection type: list
        hf: {1, 2, 3}
     end active atoms


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: MLHF wavefunction
  =======================

  - MLHF settings:

     Occupied orbitals:    Cholesky
     Virtual orbitals:     PAOs

     Cholesky decomposition threshold:  0.10E-01

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 6-31g*
        1  O    -0.573030000000     2.189950000000    -0.052560000000        1
        2  H     0.347690000000     2.485980000000     0.050490000000        2
        3  H    -1.075800000000     3.019470000000     0.020240000000        3
        4  O    -1.567030000000    -0.324500000000     0.450780000000        4
        5  H    -1.211220000000     0.588750000000     0.375890000000        5
        6  H    -1.604140000000    -0.590960000000    -0.479690000000        6
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 6-31g*
        1  O    -1.082869761160     4.138405726491    -0.099324005107        1
        2  H     0.657038876250     4.697821351146     0.095412272029        2
        3  H    -2.032967364807     5.705971341340     0.038248056761        3
        4  O    -2.961257528977    -0.613216127421     0.851850742431        4
        5  H    -2.288874076596     1.112576255838     0.710329152963        5
        6  H    -3.031385265460    -1.116752550573    -0.906482724693        6
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               38
     Number of orthonormal atomic orbitals:   38

  - Molecular orbital details:

     Number of occupied orbitals:        10
     Number of virtual orbitals:         28
     Number of molecular orbitals:       38


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a MLHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF algorithm)

  - Setting initial AO density to sad

     Energy of initial guess:              -151.779405991039
     Number of electrons in guess:           20.000000000000

  - Active orbital space:

      Number of active occupied orbitals:        5
      Number of active virtual orbitals:        18
      Number of active orbitals:                23

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-16
     Exchange screening threshold:   0.1000E-14
     ERI cutoff:                     0.1000E-16
     One-electron integral  cutoff:  0.1000E-21
     Cumulative Fock threshold:      0.1000E+01

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   none

  - Convergence thresholds

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1          -151.950801650891     0.4263E+00     0.1520E+03
     2          -151.977179401563     0.2067E+00     0.2638E-01
     3          -151.981842532438     0.1063E+00     0.4663E-02
     4          -151.983351024139     0.7176E-01     0.1508E-02
     5          -151.983883466743     0.3986E-01     0.5324E-03
     6          -151.984073735318     0.2526E-01     0.1903E-03
     7          -151.984142173864     0.1462E-01     0.6844E-04
     8          -151.984166797618     0.8995E-02     0.2462E-04
     9          -151.984175669409     0.5314E-02     0.8872E-05
    10          -151.984178865220     0.3224E-02     0.3196E-05
    11          -151.984180016875     0.1922E-02     0.1152E-05
    12          -151.984180431843     0.1159E-02     0.4150E-06
    13          -151.984180581383     0.6937E-03     0.1495E-06
    14          -151.984180635270     0.4172E-03     0.5389E-07
    15          -151.984180654689     0.2502E-03     0.1942E-07
    16          -151.984180661687     0.1503E-03     0.6998E-08
    17          -151.984180664209     0.9017E-04     0.2522E-08
    18          -151.984180665118     0.5415E-04     0.9087E-09
    19          -151.984180665445     0.3250E-04     0.3276E-09
    20          -151.984180665563     0.1951E-04     0.1180E-09
    21          -151.984180665606     0.1171E-04     0.4260E-10
    22          -151.984180665621     0.7031E-05     0.1535E-10
    23          -151.984180665627     0.4220E-05     0.5514E-11
    24          -151.984180665629     0.2534E-05     0.2103E-11
    25          -151.984180665630     0.1521E-05     0.7390E-12
    26          -151.984180665630     0.9130E-06     0.2842E-12
    27          -151.984180665630     0.5481E-06     0.5684E-13
    28          -151.984180665630     0.3290E-06     0.5684E-13
    29          -151.984180665630     0.1975E-06     0.2842E-13
    30          -151.984180665630     0.1186E-06     0.2842E-13
    31          -151.984180665630     0.7117E-07     0.5684E-13
    32          -151.984180665630     0.4273E-07     0.5684E-13
    33          -151.984180665630     0.2565E-07     0.2842E-13
    34          -151.984180665630     0.1540E-07     0.5684E-13
    35          -151.984180665630     0.9243E-08     0.5684E-13
    36          -151.984180665630     0.5548E-08     0.5684E-13
    37          -151.984180665630     0.3331E-08     0.2842E-13
    38          -151.984180665630     0.1999E-08     0.5684E-13
    39          -151.984180665630     0.1200E-08     0.8527E-13
    40          -151.984180665630     0.7205E-09     0.0000E+00
    41          -151.984180665630     0.4325E-09     0.8527E-13
    42          -151.984180665630     0.2596E-09     0.8527E-13
    43          -151.984180665630     0.1559E-09     0.5684E-13
    44          -151.984180665630     0.9358E-10     0.8527E-13
    45          -151.984180665630     0.5618E-10     0.2842E-13
    46          -151.984180665631     0.3371E-10     0.1705E-12
    47          -151.984180665631     0.2023E-10     0.2842E-13
    48          -151.984180665631     0.1217E-10     0.8527E-13
    49          -151.984180665631     0.7301E-11     0.5684E-13
  ---------------------------------------------------------------
  Convergence criterion met in 49 iterations!

  - Summary of MLHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.721516018256
     Nuclear repulsion energy:      37.386395233393
     Electronic energy:           -189.370575899024
     Total energy:                -151.984180665631

  - Summary of MLHF active/inactive contributions to electronic energy (a.u.):

     Active energy:               -104.802260473464
     Active-inactive energy:        19.260929706475
     Inactive energy:             -103.829245132035

  - Timings for the MLHF ground state calculation

     Total wall time (sec):              1.19400
     Total cpu time (sec):               1.09604

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 211.280 KB

  Total wall time in eT (sec):              1.20400
  Total cpu time in eT (sec):               1.10171

  Calculation ended: 2022-04-07 12:34:43 UTC -07:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
