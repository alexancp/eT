


                     eT 1.7 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, R. Matveeva, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, 
   E. RoncaM. Scavino, A. K. Schnack-Petersen, A. S. Skeidsvoll, Å. 
   H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.7.0 Hydra
  ------------------------------------------------------------
  Configuration date: 2022-04-07 12:33:31 UTC -07:00
  Git branch:         release-v1.7.0
  Git hash:           c5c382d7d23365463896a87c37298e4a37e4c962
  Fortran compiler:   GNU 11.2.0
  C compiler:         GNU 11.2.0
  C++ compiler:       GNU 11.2.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2022-04-07 12:34:42 UTC -07:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: 2 H2O close
        charge: 0
        multiplicity: 1
     end system

     do
        ground state
     end do

     memory
        available: 8
     end memory

     solver scf
        algorithm: mo-scf-diis
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
     end solver scf

     method
        mlhf
     end method

     active atoms
        selection type: list
        hf: {1, 2, 3}
     end active atoms

     hf mean value
        quadrupole
     end hf mean value


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: MLHF wavefunction
  =======================

  - MLHF settings:

     Occupied orbitals:    Cholesky
     Virtual orbitals:     PAOs

     Cholesky decomposition threshold:  0.10E-01

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  O    -0.573030000000     2.189950000000    -0.052560000000        1
        2  H     0.347690000000     2.485980000000     0.050490000000        2
        3  H    -1.075800000000     3.019470000000     0.020240000000        3
        4  O    -1.567030000000    -0.324500000000     0.450780000000        4
        5  H    -1.211220000000     0.588750000000     0.375890000000        5
        6  H    -1.604140000000    -0.590960000000    -0.479690000000        6
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  O    -1.082869761160     4.138405726491    -0.099324005107        1
        2  H     0.657038876250     4.697821351146     0.095412272029        2
        3  H    -2.032967364807     5.705971341340     0.038248056761        3
        4  O    -2.961257528977    -0.613216127421     0.851850742431        4
        5  H    -2.288874076596     1.112576255838     0.710329152963        5
        6  H    -3.031385265460    -1.116752550573    -0.906482724693        6
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               48
     Number of orthonormal atomic orbitals:   48

  - Molecular orbital details:

     Number of occupied orbitals:        10
     Number of virtual orbitals:         38
     Number of molecular orbitals:       48


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a MLHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (MO-SCF-DIIS algorithm)
     3) Calculate dipole and/or quadrupole moments


  1) Generate initial SAD density


  2) Calculation of reference state (MO-SCF-DIIS algorithm)

  - Setting initial AO density to sad

     Energy of initial guess:              -151.796506244373
     Number of electrons in guess:           20.000000000000

  - Active orbital space:

      Number of active occupied orbitals:        5
      Number of active virtual orbitals:        23
      Number of active orbitals:                28

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-16
     Exchange screening threshold:   0.1000E-14
     ERI cutoff:                     0.1000E-16
     One-electron integral  cutoff:  0.1000E-21
     Cumulative Fock threshold:      0.1000E+01

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1          -151.986254697203     0.3356E+00     0.1520E+03
     2          -152.011877032057     0.1366E+00     0.2562E-01
     3          -152.018448490303     0.2167E-01     0.6571E-02
     4          -152.018690017977     0.5645E-02     0.2415E-03
     5          -152.018703912212     0.1006E-02     0.1389E-04
     6          -152.018704702591     0.1890E-03     0.7904E-06
     7          -152.018704720002     0.2445E-04     0.1741E-07
     8          -152.018704720373     0.5552E-05     0.3710E-09
     9          -152.018704720397     0.1150E-05     0.2410E-10
    10          -152.018704720398     0.2301E-06     0.8242E-12
    11          -152.018704720397     0.7198E-07     0.5684E-13
    12          -152.018704720397     0.1570E-07     0.1421E-12
    13          -152.018704720397     0.2151E-08     0.8527E-13
    14          -152.018704720397     0.5725E-09     0.2842E-13
    15          -152.018704720397     0.4650E-10     0.5684E-13
    16          -152.018704720397     0.8079E-11     0.1137E-12
  ---------------------------------------------------------------
  Convergence criterion met in 16 iterations!

  - Summary of MLHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.695204742952
     Nuclear repulsion energy:      37.386395233393
     Electronic energy:           -189.405099953790
     Total energy:                -152.018704720397

  - Summary of MLHF active/inactive contributions to electronic energy (a.u.):

     Active energy:               -104.805789875468
     Active-inactive energy:        19.262564524015
     Inactive energy:             -103.861874602337


  3) Calculate dipole and/or quadrupole moments

  Warning: quadrupole moments are size-extensiveand are not well defined 
           in MLHF.

  - Operator: quadrupole moment (with trace) [a.u.]

     xx:        -3.9318625
     xy:        -0.0365902
     xz:        -0.0025058
     yy:         3.4498979
     yz:         0.5818262
     zz:        -5.2576685

  - Operator: quadrupole moment (with trace) [Debye*Ang]

     xx:        -5.2884900
     xy:        -0.0492151
     xz:        -0.0033704
     yy:         4.6402310
     yz:         0.7825762
     zz:        -7.0717446

     The traceless quadrupole is calculated as:

        Q_ij = 1/2[3*q_ij - tr(q)*delta_ij]

     where q_ij is the non-traceless matrix

  - Operator: traceless quadrupole moment [a.u.]

     xx:        -3.0279772
     xy:        -0.0548854
     xz:        -0.0037587
     yy:         8.0446634
     yz:         0.8727393
     zz:        -5.0166862

  - Operator: traceless quadrupole moment [Debye*Ang]

     xx:        -4.0727332
     xy:        -0.0738227
     xz:        -0.0050555
     yy:        10.8203483
     yz:         1.1738643
     zz:        -6.7476151

  - Timings for the MLHF ground state calculation

     Total wall time (sec):              1.36500
     Total cpu time (sec):               1.30402

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 407.904 KB

  :: There was 1 warning during the execution of eT. ::

  Total wall time in eT (sec):              1.37700
  Total cpu time in eT (sec):               1.31587

  Calculation ended: 2022-04-07 12:34:43 UTC -07:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
